/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.MergeFileDescription;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Merges datasets holding overlapping cases but different variables. The merge
 * may be controlled by keys or grouping variables.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class MergeDatasets extends TransformBase {

	public static final String TYPE = "MergeDatasets";
	/**
	 * Description of the files to be merged. (2...n)
	 */
	private MergeFileDescription[] mergeFiles;
	/**
	 * A variable or list of variables that acts as the unique case identifier
	 * across datasets. If MatchByVariables is absent, MergeType must be
	 * ""sequential"" on all files." 0..1
	 */
	private VariableReferenceBase mergeByVariables;
	/**
	 * ,The name of a variable set to 1 for the first row of each group of cases
	 * with the same value for the MatchByVariables variables and set to 0 for
	 * all other rows. 0..1
	 */
	private String firstVariable;
	/**
	 * The name of a variable set to 1 for the last row of each group of cases
	 * with the same value for the MatchByVariables variables and set to 0 for
	 * all other rows. 0..1
	 */
	private String lastVariable;
	

	public MergeFileDescription[] getMergeFiles() {
		return mergeFiles;
	}

	public void setMergeFiles(MergeFileDescription[] mergeFiles) {
		this.mergeFiles = mergeFiles;
	}

	public VariableReferenceBase getMergeByVariables() {
		return mergeByVariables;
	}

	public void setMergeByVariables(VariableReferenceBase mergeByVariables) {
		this.mergeByVariables = mergeByVariables;
	}

	public String getFirstVariable() {
		return firstVariable;
	}

	public void setFirstVariable(String firstVariable) {
		this.firstVariable = firstVariable;
	}

	public String getLastVariable() {
		return lastVariable;
	}

	public void setLastVariable(String lastVariable) {
		this.lastVariable = lastVariable;
	}

}
