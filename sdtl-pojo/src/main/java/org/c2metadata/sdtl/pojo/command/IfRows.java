package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

/**
 * A set of commands that are performed on each row in the dataframe when a
 * logical expression is true for that row. May also include ElseCommands to be
 * performed if the logical expression is false. Use DoIf for a logical
 * condition that applies to the entire dataframe and commands that are
 * performed once.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class IfRows extends TransformBase {

	public static final String TYPE = "IfRows";

	/**
	 * A logical expression that is evaluated separately for every row in the
	 * dataframe. 1...1
	 */
	private ExpressionBase condition;

	/**
	 * Commands to be performed if the condition is true. 1...n
	 * 
	 */
	private TransformBase[] thenCommands;

	/**
	 * Commands to be performed if the condition is false. 0..n
	 */
	private TransformBase[] elseCommands;

	public ExpressionBase getCondition() {
		return condition;
	}

	public void setCondition(ExpressionBase condition) {
		this.condition = condition;
	}

	public TransformBase[] getThenCommands() {
		return thenCommands;
	}

	public void setThenCommands(TransformBase[] thenCommands) {
		this.thenCommands = thenCommands;
	}

	public TransformBase[] getElseCommands() {
		return elseCommands;
	}

	public void setElseCommands(TransformBase[] elseCommands) {
		this.elseCommands = elseCommands;
	}

}
