package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.Message;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * InformBase includes information elements that do not transform data.
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, 
//defaultImpl = UnknownTransform.class, 
property = "$type", visible = true)
@JsonSubTypes({ 
		@JsonSubTypes.Type(value = Analysis.class, name = Analysis.TYPE),
		@JsonSubTypes.Type(value = Comment.class, name = Comment.TYPE),
		@JsonSubTypes.Type(value = Invalid.class, name = Invalid.TYPE),
		@JsonSubTypes.Type(value = Message.class, name = Message.TYPE),
		@JsonSubTypes.Type(value = NoTransformOp.class, name = NoTransformOp.TYPE),
		@JsonSubTypes.Type(value = Unsupported.class, name = Unsupported.TYPE) })
public abstract class InformBase extends CommandBase {

	
}
