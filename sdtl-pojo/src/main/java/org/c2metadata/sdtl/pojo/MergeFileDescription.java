package org.c2metadata.sdtl.pojo;

import java.util.List;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Describes files used in a MergeDatasets command
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonIgnoreProperties(value = { "$type" })
public class MergeFileDescription {

	/**
	 * "Name of the file being merged. May be ""Active file" 1..1
	 */
	private String fileName;
	/**
	 * Describes the type of merge performed. Enumerations: Sequential,
	 * OneToOne, ManyToOne, OneToMany, ManyToMany, OneToOne, Duplicates,
	 * Unmatched, SASmatchMerge 1..1
	 */
	private String mergeType;
	/**
	 * Creates a new variable indicating whether the row came from this file or
	 * a different input file 0..1
	 */
	private String mergeFlagVariable;
	/**
	 * Variables to be renamed, 0..n
	 */
	private RenamePair[] renameVariables;
	/**
	 * When the same variables exist in more than one dataframe. values in the
	 * “Master” dataframe can be replaced by values from a different dataframe.
	 * “Master” is the default value. “Ignore” means values in this dataframe
	 * are never used. “FillNew” is used on rows not found in the “Master”
	 * dataframe. “UpdateMissing” replaces missing values in the “Master”
	 * dataframe. “Replace” changes all values in the “Master” dataframe.” 1..1
	 */
	private String update;
	/**
	 * Generate new row when not matched to other files 1..1
	 */
	private boolean newRow;
	/**
	 * List of variables to keep. 0..n
	 */
	private VariableReferenceBase[] keepVariables;
	/**
	 * List of variables to drop. 0..n
	 */
	private VariableReferenceBase[] dropVariables;

	/**
	 * Logical condition for keeping row. 0..1
	 */
	private ExpressionBase keepCasesCondition;
	/**
	 * Logical condition for dropping row. 0..1
	 */
	private ExpressionBase dropCasesCondition;

	/**
	 * An ordered list of variables used as keys in this file to be matched to
	 * the variables in the mergeByVariables property of the MergeDatasets
	 * command. This property is only used when the key variables in this file
	 * have different names than the variable names listed in the MergeDatasets
	 * command.
	 * 
	 */
	private List<VariableReferenceBase> mergeByNames;

	/**
	 * The software package that works with the file.
	 */
	private String software;

	/**
	 * The name of a file format Valid values include: csv, txt, dat, xls, xlsx,
	 * sav, dta, sas7bdat, rds, rdata
	 */
	private String fileFormat;

	/**
	 * Indicates whether the file format is compressed.
	 */
	private boolean isCompressed;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getMergeType() {
		return mergeType;
	}

	public void setMergeType(String mergeType) {
		this.mergeType = mergeType;
	}

	public String getMergeFlagVariable() {
		return mergeFlagVariable;
	}

	public void setMergeFlagVariable(String mergeFlagVariable) {
		this.mergeFlagVariable = mergeFlagVariable;
	}

	public RenamePair[] getRenameVariables() {
		return renameVariables;
	}

	public void setRenameVariables(RenamePair[] renameVariables) {
		this.renameVariables = renameVariables;
	}

	public String getUpdate() {
		return update;
	}

	public void setUpdate(String update) {
		this.update = update;
	}

	public boolean isNewRow() {
		return newRow;
	}

	public void setNewRow(boolean newRow) {
		this.newRow = newRow;
	}

	public VariableReferenceBase[] getKeepVariables() {
		return keepVariables;
	}

	public void setKeepVariables(VariableReferenceBase[] keepVariables) {
		this.keepVariables = keepVariables;
	}

	public VariableReferenceBase[] getDropVariables() {
		return dropVariables;
	}

	public void setDropVariables(VariableReferenceBase[] dropVariables) {
		this.dropVariables = dropVariables;
	}

	public ExpressionBase getKeepCasesCondition() {
		return keepCasesCondition;
	}

	public void setKeepCasesCondition(ExpressionBase keepCasesCondition) {
		this.keepCasesCondition = keepCasesCondition;
	}

	public ExpressionBase getDropCasesCondition() {
		return dropCasesCondition;
	}

	public void setDropCasesCondition(ExpressionBase dropCasesCondition) {
		this.dropCasesCondition = dropCasesCondition;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public boolean isCompressed() {
		return isCompressed;
	}

	public void setCompressed(boolean isCompressed) {
		this.isCompressed = isCompressed;
	}

}
