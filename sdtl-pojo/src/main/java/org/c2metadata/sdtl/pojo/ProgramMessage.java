package org.c2metadata.sdtl.pojo;

import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * This is different than a regular message because it doesn't use a $type.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ProgramMessage {

	/**
	 * Information, Warning, Error
	 */
	private String severity;
	/**
	 * The line number of the source that the messages is related to, if
	 * relevant.
	 */
	private int lineNumber;
	/**
	 * The character position of the source that the message is related to, if
	 * relevant.
	 */
	private int characterPosition;

	private HashSet<String> unknownProperties;

	private String command;
	
	private SourceInformation[] sourceInformation;
	
	private String[] messageText;

	public ProgramMessage() {
		this.unknownProperties = new HashSet<>();
	}

	/**
	 * Gets the name of all unknown properties that were encountered during
	 * deserialization.
	 * 
	 * @return the unknown property names
	 */
	public String[] getUnknownProperties() {
		return unknownProperties.toArray(new String[0]);
	}

	/**
	 * A fall back setter to capture an properties that are not known so that
	 * they can be logged.
	 * 
	 * @param property
	 *            the property name
	 * @param value
	 *            the property value; this will be ignored
	 */
	@JsonAnySetter
	public void addUnknownProperty(String property, Object value) {
		unknownProperties.add(property);
	}

	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getCharacterPosition() {
		return characterPosition;
	}

	public void setCharacterPosition(int characterPosition) {
		this.characterPosition = characterPosition;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public SourceInformation[] getSourceInformation() {
		return sourceInformation;
	}

	public void setSourceInformation(SourceInformation[] sourceInformation) {
		this.sourceInformation = sourceInformation;
	}

	public String[] getMessageText() {
		return messageText;
	}

	public void setMessageText(String[] messageText) {
		this.messageText = messageText;
	}

	public void setUnknownProperties(HashSet<String> unknownProperties) {
		this.unknownProperties = unknownProperties;
	}

}
