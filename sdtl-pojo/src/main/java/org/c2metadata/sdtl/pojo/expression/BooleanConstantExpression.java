package org.c2metadata.sdtl.pojo.expression;

/**
 * BooleanConstantExpression takes values of TRUE and FALSE.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class BooleanConstantExpression extends ExpressionBase {

	public static final String TYPE = "BooleanConstantExpression";

	/**
	 * Values of TRUE and FALSE are used in logical expressions.
	 * 
	 * 1...1
	 */
	private boolean booleanValue;


	public boolean isBooleanValue() {
		return booleanValue;
	}

	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
	
	
}
