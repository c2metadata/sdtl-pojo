package org.c2metadata.sdtl.pojo.command;

/**
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NewDataframe extends TransformBase {

	public static final String TYPE = "NewDataframe";

	/**
	 * Number of rows in the new dataframe
	 */
	private int numberOfRows;

	/**
	 * Number of columns in the new dataframe
	 */
	private int numberOfColumns;

	public int getNumberOfRows() {
		return numberOfRows;
	}

	public void setNumberOfRows(int numberOfRows) {
		this.numberOfRows = numberOfRows;
	}

	public int getNumberOfColumns() {
		return numberOfColumns;
	}

	public void setNumberOfColumns(int numberOfColumns) {
		this.numberOfColumns = numberOfColumns;
	}

}
