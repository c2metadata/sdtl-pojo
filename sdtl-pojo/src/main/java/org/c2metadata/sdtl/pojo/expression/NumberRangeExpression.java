package org.c2metadata.sdtl.pojo.expression;

/**
 * Defines a range of numeric values
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NumberRangeExpression extends ExpressionBase {

	public static final String TYPE = "NumberRangeExpression";
	/**
	 * Starting value for range. 1..1
	 */
	private ExpressionBase numberRangeStart;
	/**
	 * Ending value for range. 1..1
	 */
	private ExpressionBase numberRangeEnd;
	/**
	 * Increment for stepping through range. 0..1
	 */
	private ExpressionBase numberRangeIncrement;

	public ExpressionBase getNumberRangeStart() {
		return numberRangeStart;
	}

	public void setNumberRangeStart(ExpressionBase rangeStart) {
		this.numberRangeStart = rangeStart;
	}

	public ExpressionBase getNumberRangeEnd() {
		return numberRangeEnd;
	}

	public void setNumberRangeEnd(ExpressionBase rangeEnd) {
		this.numberRangeEnd = rangeEnd;
	}

	public ExpressionBase getNumberRangeIncrement() {
		return numberRangeIncrement;
	}

	public void setNumberRangeIncrement(ExpressionBase rangeIncrement) {
		this.numberRangeIncrement = rangeIncrement;
	}

}
