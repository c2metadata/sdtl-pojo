package org.c2metadata.sdtl.pojo;

import java.util.ArrayList;
import java.util.List;

import org.c2metadata.sdtl.pojo.command.AppendDatasets;
import org.c2metadata.sdtl.pojo.command.InformBase;
import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Describes files used in an {@link AppendDatasets} command.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonIgnoreProperties(value = { "$type" })
public class AppendFileDescription extends InformBase{
	
	/**
	 * Name of the file being appended. May be the name of an active dataframe.
	 * 
	 * 1...1
	 */
	private String fileName;
	/**
	 * Variables to be renamed
	 * 
	 * 0...n
	 */
	private List<RenamePair> renameVariables;
	/**
	 * List of variables to keep
	 * 
	 * 0...n
	 */
	private List<VariableReferenceBase> keepVariables;
	/**
	 * List of variables to drop.
	 * 
	 * 0...n
	 */
	private List<VariableReferenceBase> dropVariables;

	/**
	 * Logical condition for keeping rows. 0..1
	 */
	private ExpressionBase keepCasesCondition;

	/**
	 * Logical condition for dropping rows. 0..1
	 */
	private ExpressionBase dropCasesCondition;
	
	/**
	 * The software package that works with the file.
	 */
	private String software;
	
	/**
	 * The name of a file format Valid values include: csv, txt, dat, xls, xlsx, sav, dta, sas7bdat, rds, rdata
	 */
	private String fileFormat;
	
	/**
	 * Indicates whether the file format is compressed. (optional)
	 */
	private Boolean isCompressed;

	public AppendFileDescription() {
		this.renameVariables = new ArrayList<>();
		this.keepVariables = new ArrayList<>();
		this.dropVariables = new ArrayList<>();
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public List<RenamePair> getRenameVariables() {
		return renameVariables;
	}

	public void setRenameVariables(List<RenamePair> renameVariables) {
		this.renameVariables = renameVariables;
	}

	public List<VariableReferenceBase> getKeepVariables() {
		return keepVariables;
	}

	public void setKeepVariables(List<VariableReferenceBase> keepVariables) {
		this.keepVariables = keepVariables;
	}

	public List<VariableReferenceBase> getDropVariables() {
		return dropVariables;
	}

	public void setDropVariables(List<VariableReferenceBase> dropVariables) {
		this.dropVariables = dropVariables;
	}

	public ExpressionBase getKeepCasesCondition() {
		return keepCasesCondition;
	}

	public void setKeepCasesCondition(ExpressionBase keepCasesCondition) {
		this.keepCasesCondition = keepCasesCondition;
	}

	public ExpressionBase getDropCasesCondition() {
		return dropCasesCondition;
	}

	public void setDropCasesCondition(ExpressionBase dropCasesCondition) {
		this.dropCasesCondition = dropCasesCondition;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public Boolean isCompressed() {
		return isCompressed;
	}

	public void setCompressed(Boolean isCompressed) {
		this.isCompressed = isCompressed;
	}
	
	
}
