/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.DataframeDescription;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * TransformBase defines general properties available on all transform commands.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us) 
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, 
//defaultImpl = UnknownTransform.class, 
property = "$type", visible = true)
@JsonSubTypes({ @JsonSubTypes.Type(value = Aggregate.class, name = Aggregate.TYPE),
//		@JsonSubTypes.Type(value = Analysis.class, name = Analysis.TYPE),
		@JsonSubTypes.Type(value = AppendDatasets.class, name = AppendDatasets.TYPE),
		@JsonSubTypes.Type(value = Collapse.class, name = Collapse.TYPE),
//		@JsonSubTypes.Type(value = Comment.class, name = Comment.TYPE),
		@JsonSubTypes.Type(value = Compute.class, name = Compute.TYPE),
		@JsonSubTypes.Type(value = DropVariables.class, name = DropVariables.TYPE),
		@JsonSubTypes.Type(value = DoIf.class, name = DoIf.TYPE),
		@JsonSubTypes.Type(value = DropCases.class, name = DropCases.TYPE),
		@JsonSubTypes.Type(value = Execute.class, name = Execute.TYPE),
		@JsonSubTypes.Type(value = IfRows.class, name = IfRows.TYPE),
//		@JsonSubTypes.Type(value = Invalid.class, name = Invalid.TYPE),
		@JsonSubTypes.Type(value = KeepCases.class, name = KeepCases.TYPE),
		@JsonSubTypes.Type(value = KeepVariables.class, name = KeepVariables.TYPE),
		@JsonSubTypes.Type(value = Load.class, name = Load.TYPE),
		@JsonSubTypes.Type(value = LoopOverList.class, name = LoopOverList.TYPE),
		@JsonSubTypes.Type(value = LoopWhile.class, name = LoopWhile.TYPE),
		@JsonSubTypes.Type(value = MergeDatasets.class, name = MergeDatasets.TYPE),
		@JsonSubTypes.Type(value = NewDataframe.class, name = NewDataframe.TYPE),
//		@JsonSubTypes.Type(value = NoTransformOp.class, name = NoTransformOp.TYPE),
		@JsonSubTypes.Type(value = Recode.class, name = Recode.TYPE),
		@JsonSubTypes.Type(value = Rename.class, name = Rename.TYPE),
		@JsonSubTypes.Type(value = ReshapeLong.class, name = ReshapeLong.TYPE),
		@JsonSubTypes.Type(value = ReshapeWide.class, name = ReshapeWide.TYPE),
		@JsonSubTypes.Type(value = Save.class, name = Save.TYPE),
		@JsonSubTypes.Type(value = SetDatasetProperty.class, name = SetDatasetProperty.TYPE),
		@JsonSubTypes.Type(value = SetDataType.class, name = SetDataType.TYPE),
		@JsonSubTypes.Type(value = SetDisplayFormat.class, name = SetDisplayFormat.TYPE),
		@JsonSubTypes.Type(value = SetMissingValues.class, name = SetMissingValues.TYPE),
		@JsonSubTypes.Type(value = SetValueLabels.class, name = SetValueLabels.TYPE),
		@JsonSubTypes.Type(value = SetVariableLabel.class, name = SetVariableLabel.TYPE),
		@JsonSubTypes.Type(value = SortCases.class, name = SortCases.TYPE),
//		@JsonSubTypes.Type(value = Unsupported.class, name = Unsupported.TYPE) 
		})
public abstract class TransformBase extends CommandBase{

	/**
	 * Signify the dataframe which this transform produces. (0...n)
	 */
	private DataframeDescription[] producesDataframe;

	/**
	 * Signify the dataframe which this transform acts upon. (0...n)
	 */
	private DataframeDescription[] consumesDataframe;


	public DataframeDescription[] getProducesDataframe() {
		return producesDataframe;
	}

	public void setProducesDataframe(DataframeDescription[] producesDataframe) {
		this.producesDataframe = producesDataframe;
	}

	public DataframeDescription[] getConsumesDataframe() {
		return consumesDataframe;
	}

	public void setConsumesDataframe(DataframeDescription[] consumesDataframe) {
		this.consumesDataframe = consumesDataframe;
	}

	
}
