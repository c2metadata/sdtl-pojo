package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.Weight;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

/**
 * A collapse command summarizes data using aggregation functions applied to
 * data that may be grouped by one or more variables. The resulting summary data
 * is represented in a new dataset. See the collapse functions in the function
 * library.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Collapse extends TransformBase {

	public static final String TYPE = "Collapse";

	/**
	 * The name of a new, aggregated dataset to be created. 0..1
	 */
	private String outputDatasetName;
	/**
	 * Variables used as keys to identify groups. 0..n
	 */
	private VariableReferenceBase[] groupByVariables;
	/**
	 * The expressions that compute the aggregations. An aggregation function
	 * should be used. 1..n
	 */
	private Compute[] aggregateVariables;

	/**
	 * Description of the weights to be used. 0..1
	 */
	private Weight weighting;

	public String getOutputDatasetName() {
		return outputDatasetName;
	}

	public void setOutputDatasetName(String outputDatasetName) {
		this.outputDatasetName = outputDatasetName;
	}

	public VariableReferenceBase[] getGroupByVariables() {
		return groupByVariables;
	}

	public void setGroupByVariables(VariableReferenceBase[] groupByVariables) {
		this.groupByVariables = groupByVariables;
	}

	public Compute[] getAggregateVariables() {
		return aggregateVariables;
	}

	public void setAggregateVariables(Compute[] aggregateVariables) {
		this.aggregateVariables = aggregateVariables;
	}

	public Weight getWeighting() {
		return weighting;
	}

	public void setWeighting(Weight weighting) {
		this.weighting = weighting;
	}
}
