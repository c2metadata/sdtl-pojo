package org.c2metadata.sdtl.pojo.command;

import java.util.HashSet;

import org.c2metadata.sdtl.pojo.Message;
import org.c2metadata.sdtl.pojo.SourceInformation;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * CommandBase defines general properties available to all commands.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, defaultImpl = UnknownTransform.class, property = "$type", visible = true)
@JsonSubTypes({ @JsonSubTypes.Type(value = Aggregate.class, name = Aggregate.TYPE),
		@JsonSubTypes.Type(value = Analysis.class, name = Analysis.TYPE),
		@JsonSubTypes.Type(value = AppendDatasets.class, name = AppendDatasets.TYPE),
		@JsonSubTypes.Type(value = Collapse.class, name = Collapse.TYPE),
		@JsonSubTypes.Type(value = Comment.class, name = Comment.TYPE),
		@JsonSubTypes.Type(value = Compute.class, name = Compute.TYPE),
		@JsonSubTypes.Type(value = DropVariables.class, name = DropVariables.TYPE),
		@JsonSubTypes.Type(value = DoIf.class, name = DoIf.TYPE),
		@JsonSubTypes.Type(value = DropCases.class, name = DropCases.TYPE),
		@JsonSubTypes.Type(value = Execute.class, name = Execute.TYPE),
		@JsonSubTypes.Type(value = IfRows.class, name = IfRows.TYPE),
		@JsonSubTypes.Type(value = Invalid.class, name = Invalid.TYPE),
		@JsonSubTypes.Type(value = KeepCases.class, name = KeepCases.TYPE),
		@JsonSubTypes.Type(value = KeepVariables.class, name = KeepVariables.TYPE),
		@JsonSubTypes.Type(value = Load.class, name = Load.TYPE),
		@JsonSubTypes.Type(value = LoopOverList.class, name = LoopOverList.TYPE),
		@JsonSubTypes.Type(value = LoopWhile.class, name = LoopWhile.TYPE),
		@JsonSubTypes.Type(value = MergeDatasets.class, name = MergeDatasets.TYPE),
		 @JsonSubTypes.Type(value = Message.class, name = Message.TYPE),
		@JsonSubTypes.Type(value = NewDataframe.class, name = NewDataframe.TYPE),
		@JsonSubTypes.Type(value = NoTransformOp.class, name = NoTransformOp.TYPE),
		@JsonSubTypes.Type(value = Recode.class, name = Recode.TYPE),
		@JsonSubTypes.Type(value = Rename.class, name = Rename.TYPE),
		@JsonSubTypes.Type(value = ReshapeLong.class, name = ReshapeLong.TYPE),
		@JsonSubTypes.Type(value = ReshapeWide.class, name = ReshapeWide.TYPE),
		@JsonSubTypes.Type(value = Save.class, name = Save.TYPE),
		@JsonSubTypes.Type(value = SetDatasetProperty.class, name = SetDatasetProperty.TYPE),
		@JsonSubTypes.Type(value = SetDataType.class, name = SetDataType.TYPE),
		@JsonSubTypes.Type(value = SetDisplayFormat.class, name = SetDisplayFormat.TYPE),
		@JsonSubTypes.Type(value = SetMissingValues.class, name = SetMissingValues.TYPE),
		@JsonSubTypes.Type(value = SetValueLabels.class, name = SetValueLabels.TYPE),
		@JsonSubTypes.Type(value = SetVariableLabel.class, name = SetVariableLabel.TYPE),
		@JsonSubTypes.Type(value = SortCases.class, name = SortCases.TYPE),
		@JsonSubTypes.Type(value = Unsupported.class, name = Unsupported.TYPE),
		@JsonSubTypes.Type(value = UnknownTransform.class, name = UnknownTransform.TYPE)

})
public class CommandBase {
	/**
	 * A set to contain the names of unknown properties.
	 */
	private HashSet<String> unknownProperties;
	/**
	 * The type of command
	 */
	private String command;
	/**
	 * Information about the source of the command.
	 */
	private SourceInformation[] sourceInformation;
	/**
	 * Adds a message that can be displayed with the command.
	 */
	private String[] messageText;
	/**
	 * type used for deserialization
	 */
	private String type;

	public CommandBase() {
		unknownProperties = new HashSet<>();
	}

	// need to specify this so that type doesn't end up in "unknown properties"
	@JsonProperty("$type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}

	public SourceInformation[] getSourceInformation() {
		return sourceInformation;
	}

	public void setSourceInformation(SourceInformation[] sourceInformation) {
		this.sourceInformation = sourceInformation;
	}

	public String[] getMessageText() {
		return messageText;
	}

	public void setMessageText(String[] messageText) {
		this.messageText = messageText;
	}

	/**
	 * Gets the name of all unknown properties that were encountered during
	 * deserialization.
	 * 
	 * @return the unknown property names
	 */
	public String[] getUnknownProperties() {
		return unknownProperties.toArray(new String[0]);
	}

	/**
	 * A fall back setter to capture an properties that are not known so that
	 * they can be logged.
	 * 
	 * @param property
	 *            the property name
	 * @param value
	 *            the property value; this will be ignored
	 */
	@JsonAnySetter
	public void addUnknownProperty(String property, Object value) {
		unknownProperties.add(property);
	}

	@JsonIgnore
	public String getSourceInformationConcatenatedCommandText() {
		StringBuilder sb = new StringBuilder();
		if (sourceInformation == null) {
			return "No source information found on this command";
			// throw new RuntimeJsonMappingException("No Source Information
			// Array found on the command - please check that SDTL is valid.");
		}
		for (SourceInformation si : sourceInformation) {
			sb.append(si.getOriginalSourceText()).append(", ");
		}
		if (sb.length() > 2) {
			sb.delete(sb.length() - 2, sb.length() - 1);
		}
		return sb.toString();
	}

}
