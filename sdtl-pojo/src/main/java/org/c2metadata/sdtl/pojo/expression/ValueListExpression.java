package org.c2metadata.sdtl.pojo.expression;
/**
 * Wraps a list of other expressions.
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ValueListExpression extends ExpressionBase{

	public static final String TYPE = "ValueListExpression";

	/**
	 * the list of expressions. 1..n
	 */
	private ExpressionBase[] values;

	public ExpressionBase[] getValues() {
		return values;
	}

	public void setValues(ExpressionBase[] values) {
		this.values = values;
	}
	
	
}
