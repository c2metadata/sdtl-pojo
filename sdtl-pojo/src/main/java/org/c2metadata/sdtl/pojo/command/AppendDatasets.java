package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.AppendFileDescription;

/**
 * Combines datasets by concatenation for datasets with the same or overlapping
 * variables.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class AppendDatasets extends TransformBase {

	public static final String TYPE = "AppendDatasets";

	/**
	 * Description of files to be appended
	 * 
	 * 1...n
	 */
	private AppendFileDescription[] appendFiles;

	/**
	 * Creates a new variable identifying the source dataframe for a row with
	 * the variable name given in the value of the property
	 * 
	 * 0...1
	 */
	private String appendFlagVariable;

	public AppendDatasets() {
	}

	public AppendFileDescription[] getAppendFiles() {
		return appendFiles;
	}
	
	public void setAppendFiles(AppendFileDescription[] appendFiles) {
		this.appendFiles = appendFiles;
	}

	public String getAppendFlagVariable() {
		return appendFlagVariable;
	}

	public void setAppendFlagVariable(String appendFlagVariable) {
		this.appendFlagVariable = appendFlagVariable;
	}

}
