package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * Describes the arguments in a function as specified in the SDTL Function
 * Library.
 * 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "$type")
public class FunctionArgument extends ExpressionBase {

	public static final String TYPE = "FunctionArgument";

	/**
	 * the name of the parameter.
	 */
	private String argumentName;
	/**
	 * the value of the parameter. 1..1
	 */
	private ExpressionBase argumentValue;

	public String getArgumentName() {
		return argumentName;
	}

	public void setArgumentName(String argumentName) {
		this.argumentName = argumentName;
	}

	public ExpressionBase getArgumentValue() {
		return argumentValue;
	}

	public void setArgumentValue(ExpressionBase argumentValue) {
		this.argumentValue = argumentValue;
	}

}
