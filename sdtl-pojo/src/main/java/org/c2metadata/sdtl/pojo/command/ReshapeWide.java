/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.ReshapeItemDescription;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * ReshapeWide is not supported in the current version of SDTL, because it
 * depends on values in the data. However, it may be useful when values of the
 * index variable are available in the metadata file or the data can be
 * processed.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ReshapeWide extends TransformBase {

	public static final String TYPE = "ReshapeWide";
	/**
	 * New variables created by this command.
	 */
	private ReshapeItemDescription[] makeItems;
	/**
	 * One or more variables identifying unique rows in the wide data.
	 */
	private VariableReferenceBase iDVariables;
	/**
	 * Variables to be dropped from the new dataset.
	 */
	private VariableReferenceBase dropVariables;
	/**
	 * Variables to be kept in the new dataset
	 */
	private VariableReferenceBase[] keepVariables;

	public ReshapeItemDescription[] getMakeItems() {
		return makeItems;
	}

	public void setMakeItems(ReshapeItemDescription[] makeItems) {
		this.makeItems = makeItems;
	}

	public VariableReferenceBase getiDVariables() {
		return iDVariables;
	}

	public void setiDVariables(VariableReferenceBase iDVariables) {
		this.iDVariables = iDVariables;
	}

	public VariableReferenceBase getDropVariables() {
		return dropVariables;
	}

	public void setDropVariables(VariableReferenceBase dropVariables) {
		this.dropVariables = dropVariables;
	}

	public VariableReferenceBase[] getKeepVariables() {
		return keepVariables;
	}

	public void setKeepVariables(VariableReferenceBase[] keepVariables) {
		this.keepVariables = keepVariables;
	}

}
