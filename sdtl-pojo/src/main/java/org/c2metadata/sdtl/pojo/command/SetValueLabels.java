/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.ValueLabel;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

public class SetValueLabels extends TransformBase {

	public static final String TYPE = "SetValueLabels";

	/**
	 * The names of the variables to which a label will be assigned. 1..n
	 */
	private VariableReferenceBase[] variables;

	/**
	 * The label to be assigned to the variables. 1..n
	 */
	private ValueLabel[] labels;

	public VariableReferenceBase[] getVariables() {
		return variables;
	}

	public void setVariables(VariableReferenceBase[] variables) {
		this.variables = variables;
	}

	public ValueLabel[] getLabels() {
		return labels;
	}

	public void setLabels(ValueLabel[] labels) {
		this.labels = labels;
	}

}
