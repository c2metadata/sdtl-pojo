package org.c2metadata.sdtl.pojo.expression;

/**
 * A composite variable name is used to describe a variable name that is
 * computed.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class CompositeVariableNameExpression extends VariableReferenceBase {
	
	public static final String TYPE = "CompositeVariableNameExpression";

	/**
	 * An expression that evaluates to the constructed variable name.
	 */
	private ExpressionBase computedVariableName;

	public ExpressionBase getComputedVariableName() {
		return computedVariableName;
	}

	public void setComputedVariableName(ExpressionBase computedVariableName) {
		this.computedVariableName = computedVariableName;
	}

}
