/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo;

/**
 * Describes a variable that will have its values recoded.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class RecodeVariable {
	/**
	 * The name of the variable which will have its values recoded. Could be
	 * VariableSymbolExpression, VariableRangeExpression,
	 * VariableListExpression, CompositeVariableName.
	 */
	private String source;
	/**
	 * The name of the new variable into which the recoded values are inserted.
	 * This may be the same as the source variable if values are recoded in
	 * place.
	 */
	private String target;

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public String getTarget() {
		return target;
	}

	public void setTarget(String target) {
		this.target = target;
	}

}
