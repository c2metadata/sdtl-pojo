package org.c2metadata.sdtl.pojo;

import java.util.ArrayList;
import java.util.List;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Provides a name for the dataframe and an ordered list of variables
 * (optional).
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DataframeDescription {

	/**
	 * Name of the dataframe.
	 * 
	 */
	private String dataframeName;

	/**
	 * Variables in the dataframe in order
	 */
	private String[] variableInventory;

	/**
	 * An ordered list of variables used as hierarchical dimensions in a data
	 * cube or multi-index
	 * 
	 */
	private List<VariableReferenceBase> rowDimensions;

	/**
	 * An ordered list of variables used as column indexes in a multi-index
	 * 
	 */
	private List<VariableReferenceBase> columnDimensions;

	public DataframeDescription() {
		this.rowDimensions = new ArrayList<>();
		this.columnDimensions = new ArrayList<>();
	}

	public String getDataframeName() {
		return dataframeName;
	}

	public void setDataframeName(String dataframeName) {
		this.dataframeName = dataframeName;
	}

	public String[] getVariableInventory() {
		return variableInventory;
	}

	public void setVariableInventory(String[] variableInventory) {
		this.variableInventory = variableInventory;
	}

	public List<VariableReferenceBase> getRowDimensions() {
		return rowDimensions;
	}

	public void setRowDimensions(List<VariableReferenceBase> rowDimensions) {
		this.rowDimensions = rowDimensions;
	}

	public List<VariableReferenceBase> getColumnDimensions() {
		return columnDimensions;
	}

	public void setColumnDimensions(List<VariableReferenceBase> columnDimensions) {
		this.columnDimensions = columnDimensions;
	}

}
