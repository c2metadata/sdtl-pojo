package org.c2metadata.sdtl.pojo.expression;

import java.time.LocalDateTime;

public class DateTimeConstant extends ExpressionBase {
	
	public static final String TYPE = "DateTimeConstant";

	/**
	 * A specific instance of time in ISO 8601 format
	 */
	private LocalDateTime dateTimeValue;

	public DateTimeConstant() {
		// TODO Auto-generated constructor stub
	}

	public LocalDateTime getDateTimeValue() {
		return dateTimeValue;
	}

	public void setDateTimeValue(LocalDateTime dateTimeValue) {
		this.dateTimeValue = dateTimeValue;
	}
}
