package org.c2metadata.sdtl.pojo.expression;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "typeName")
public class UnsupportedExpression extends ExpressionBase {

	public static final String TYPE = "UnsupportedExpression";
	
}
