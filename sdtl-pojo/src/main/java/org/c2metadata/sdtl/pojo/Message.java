package org.c2metadata.sdtl.pojo;

import java.util.HashSet;

import org.c2metadata.sdtl.pojo.command.InformBase;

import com.fasterxml.jackson.annotation.JsonAnySetter;

/**
 * Inserts message text in the SDTL file.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Message extends InformBase {

	public static final String TYPE = "Message";

	/**
	 * Information, Warning, Error
	 */
	private String severity;
	/**
	 * The line number of the source that the messages is related to, if
	 * relevant.
	 */
	private int lineNumber;
	/**
	 * The character position of the source that the message is related to, if
	 * relevant.
	 */
	private int characterPosition;

	private HashSet<String> unknownProperties;

	/**
	 * type used for deserialization
	 */
	public Message() {
		this.unknownProperties = new HashSet<>();
	}

	/**
	 * Gets the name of all unknown properties that were encountered during
	 * deserialization.
	 * 
	 * @return the unknown property names
	 */
	public String[] getUnknownProperties() {
		return unknownProperties.toArray(new String[0]);
	}

	/**
	 * A fall back setter to capture an properties that are not known so that
	 * they can be logged.
	 * 
	 * @param property
	 *            the property name
	 * @param value
	 *            the property value; this will be ignored
	 */
	@JsonAnySetter
	public void addUnknownProperty(String property, Object value) {
		unknownProperties.add(property);
	}


	public String getSeverity() {
		return severity;
	}

	public void setSeverity(String severity) {
		this.severity = severity;
	}

	public int getLineNumber() {
		return lineNumber;
	}

	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	public int getCharacterPosition() {
		return characterPosition;
	}

	public void setCharacterPosition(int characterPosition) {
		this.characterPosition = characterPosition;
	}

}
