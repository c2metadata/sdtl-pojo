package org.c2metadata.sdtl.pojo.expression;

/**
 * An expression that represents all variables in the dataset, similar to _all
 * in SPSS or Stata.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class AllVariablesExpression extends VariableReferenceBase{
	public static final String TYPE = "AllVariablesExpression";

}
