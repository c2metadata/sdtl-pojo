package org.c2metadata.sdtl.pojo.expression;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 *            ***THIS IS NOT PART OF THE COGS MODEL. ***
 * 
 * This was added to be the default deserialization for any expression that does
 * not match up with a subtype listed on {@link ExpressionBase}. Any properties
 * that are not type or value will be ignored.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NONE)
public class UnknownExpression extends ExpressionBase {

}
