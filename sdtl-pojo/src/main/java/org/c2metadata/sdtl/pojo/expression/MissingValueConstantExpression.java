package org.c2metadata.sdtl.pojo.expression;

/**
 * A missing value constant. Some languages allow multiple missing value
 * constants.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class MissingValueConstantExpression extends ExpressionBase {

	public static final String TYPE = "MissingValueConstantExpression";

	/**
	 * The missing value as it appears in the system (e.g., .a, .b, .c). If
	 * null, this is the system missing value.
	 */
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
