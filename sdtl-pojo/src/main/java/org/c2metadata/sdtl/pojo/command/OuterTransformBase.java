package org.c2metadata.sdtl.pojo.command;

/**
 * This class exists to hold an encompassing transform while processing a nested
 * transform. Use case: in an ifRows command, there is a place for
 * "thenCommands" and "elseCommands" which are lists of transformBAses. These
 * transforms need to be processed like normal, but should always be shown in
 * the derivation as part of their parent transform, the IfRows. This
 * OuterTransform would allow us to put the IfRows command here, as the
 * outerTransform, so it is accessible while we process the inner transform.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class OuterTransformBase extends CommandBase {
	
	private TransformBase outerTransform;
	
	private TransformBase innerTransform;

	public OuterTransformBase() {
	}

	public TransformBase getOuterTransform() {
		return outerTransform;
	}

	public void setOuterTransform(TransformBase outerTransform) {
		this.outerTransform = outerTransform;
	}

	public TransformBase getInnerTransform() {
		return innerTransform;
	}

	public void setInnerTransform(TransformBase innerTransform) {
		this.innerTransform = innerTransform;
	}
}
