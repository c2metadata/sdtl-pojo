package org.c2metadata.sdtl.pojo.command;

/**
 * NoTransformOp is used for a command in the original script that provides
 * important information but does not have a function in SDTL. For example,
 * “library()” in R loads a package of R functions. Since the Parser detects the
 * library, the SDTL will reflect the library that is used, and commands derived
 * from the library will be translated in the SDTL script. However, it is useful
 * to know which library is active for auditing the R script, even if it does
 * not perform any data transformations.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NoTransformOp extends InformBase{
	
	public static final String TYPE = "NoTransformOp";

}
