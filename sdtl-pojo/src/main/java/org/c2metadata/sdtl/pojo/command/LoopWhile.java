package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

/**
 * LoopWhile iterates over a set of commands under the control of one or more
 * logical expressions. Since the logical conditions typically depend upon
 * values in the data, commands executed in a LoopWhile cannot be anticipated
 * and expanded in SDTL.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class LoopWhile extends TransformBase {

	public static final String TYPE = "LoopWhile";
	/**
	 * ,Describes a condition required for the next iteration to begin. 0..1
	 */
	private ExpressionBase condition;
	/**
	 * Describes a condition that ends iteration. 0..1
	 */
	private ExpressionBase endCondition;
	/**
	 * Commands within the loop 1..n
	 */
	private TransformBase[] commands;

	/**
	 * When true, the loop has been expanded into separate commands.
	 */
	private boolean updated;

	public ExpressionBase getCondition() {
		return condition;
	}

	public void setCondition(ExpressionBase condition) {
		this.condition = condition;
	}

	public ExpressionBase getEndCondition() {
		return endCondition;
	}

	public void setEndCondition(ExpressionBase endCondition) {
		this.endCondition = endCondition;
	}

	public TransformBase[] getCommands() {
		return commands;
	}

	public void setCommands(TransformBase[] commands) {
		this.commands = commands;
	}

	public boolean isUpdated() {
		return updated;
	}

	public void setUpdated(boolean updated) {
		this.updated = updated;
	}

}
