/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.ReshapeItemDescription;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Creates a new dataset with multiple rows per case by assigning a set of
 * variables in the original dataset to a single variable in the new dataset.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ReshapeLong extends TransformBase {

	public static final String TYPE = "ReshapeLong";

	/**
	 * New variables created by this command. 0..n
	 */
	private ReshapeItemDescription[] makeItems;
	/**
	 * New variable identifying the case number in the wide data that created
	 * this row. 0..1
	 */
	private String caseNumberVariable;
	/**
	 * One or more variables identifying unique rows in the wide data. 0..1
	 */
	private VariableReferenceBase idVariables;
	/**
	 * Variables to be dropped from the new dataset. 0..1
	 */
	private VariableReferenceBase dropVariables;
	/**
	 * Variables to be kept in the new dataset. 0..1
	 */
	private VariableReferenceBase keepVariables;
	/**
	 * When set to TRUE, rows in which all constructed variables are missing are
	 * not deleted.
	 */
	private boolean keepNullCases;
	/**
	 * New variable with the number of cases in the long dataset that were
	 * created from the source row in the wide dataset. 0..1
	 */
	private String countByID;
	/**
	 * Label for the CountByID variable. 0..1
	 */
	private String countByIDLabel;

	public ReshapeItemDescription[] getMakeItems() {
		return makeItems;
	}

	public void setMakeItems(ReshapeItemDescription[] makeItems) {
		this.makeItems = makeItems;
	}

	public String getCaseNumberVariable() {
		return caseNumberVariable;
	}

	public void setCaseNumberVariable(String caseNumberVariable) {
		this.caseNumberVariable = caseNumberVariable;
	}

	public VariableReferenceBase getIdVariables() {
		return idVariables;
	}

	public void setIdVariables(VariableReferenceBase idVariables) {
		this.idVariables = idVariables;
	}

	public VariableReferenceBase getDropVariables() {
		return dropVariables;
	}

	public void setDropVariables(VariableReferenceBase dropVariables) {
		this.dropVariables = dropVariables;
	}

	public VariableReferenceBase getKeepVariables() {
		return keepVariables;
	}

	public void setKeepVariables(VariableReferenceBase keepVariables) {
		this.keepVariables = keepVariables;
	}

	public boolean isKeepNullCases() {
		return keepNullCases;
	}

	public void setKeepNullCases(boolean keepNullCases) {
		this.keepNullCases = keepNullCases;
	}

	public String getCountByID() {
		return countByID;
	}

	public void setCountByID(String countByID) {
		this.countByID = countByID;
	}

	public String getCountByIDLabel() {
		return countByIDLabel;
	}

	public void setCountByIDLabel(String countByIDLabel) {
		this.countByIDLabel = countByIDLabel;
	}

}
