package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

/**
 * Weight describes the type of weight to apply and variables holding weights.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Weight {

	/**
	 * The variable used as a weight in the operation. 0...1
	 */
	private VariableSymbolExpression weightVraiable;

	/**
	 * Type of weight to use Valid values include: frequency, probability,
	 * Stata_aweight, Stata_iweight. 0...1
	 */
	private String weightType;

	public VariableSymbolExpression getWeightVraiable() {
		return weightVraiable;
	}

	public void setWeightVraiable(VariableSymbolExpression weightVraiable) {
		this.weightVraiable = weightVraiable;
	}

	public String getWeightType() {
		return weightType;
	}

	public void setWeightType(String weightType) {
		this.weightType = weightType;
	}

}
