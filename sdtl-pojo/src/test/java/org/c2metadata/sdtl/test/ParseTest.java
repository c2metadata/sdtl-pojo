package org.c2metadata.sdtl.test;

import java.io.File;
import java.io.FilenameFilter;

import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.c2metadata.sdtl.pojo.Program;
import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.junit.Before;
import org.junit.Test;

import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ParseTest {

	private ObjectMapper mapper;

	@Before
	public void setUp() {
		mapper = new ObjectMapper();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
	}
	
//	@Ignore //out of date sdtl
	@Test
	public void testSdtlJsonSamples() {
		readDirectory("SDTL Samples", new File("src/test/resources/sdtlJson"));
//		Assert.assertTrue(readDirectory("SDTL Samples", new File("src/test/resources/sdtlJson")));
	}
	

	private boolean readDirectory(String name, File directory) {
		boolean errors = false;
		System.out.println("=== " + name + " ===");
		for (File json : directory.listFiles((FilenameFilter) new SuffixFileFilter(".json"))) {
			System.out.println("File: " + json.getAbsolutePath());
			try {
				for (CommandBase command : mapper.readValue(json, Program.class).getCommands()) {
					System.out.println("\t" + command.getCommand() + ": " + command.getClass().getName());
					for (String property : command.getUnknownProperties()) {
						System.out.println("\t\tUnknown property: " + property);
					}
				}
				System.out.println();
			} catch (Exception e) {
				errors = true;
				System.err.println("Error parsing " + json.getAbsolutePath());
				e.printStackTrace(System.err);
			}
		}
		System.out.println();
		return !errors;
	}

}
