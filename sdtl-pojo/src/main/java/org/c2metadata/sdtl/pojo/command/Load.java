/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

public class Load extends TransformBase {

	public static final String TYPE = "Load";

	/**
	 * The name of the file. 1..1
	 */
	private String fileName;
	/**
	 * The software package that works with the file.
	 */
	private String software;

	/**
	 * The name of a file format Valid values include: csv, txt, dat, xls, xlsx,
	 * sav, dta, sas7bdat, rds, rdata
	 */
	private String fileFormat;

	/**
	 * Indicates whether the file format is compressed.
	 */
	private boolean isCompressed;

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getSoftware() {
		return software;
	}

	public void setSoftware(String software) {
		this.software = software;
	}

	public boolean getIsCompressed() {
		return isCompressed;
	}

	public void setIsCompressed(boolean isCompressed) {
		this.isCompressed = isCompressed;
	}

	public String getFileFormat() {
		return fileFormat;
	}

	public void setFileFormat(String fileFormat) {
		this.fileFormat = fileFormat;
	}

	public void setCompressed(boolean isCompressed) {
		this.isCompressed = isCompressed;
	}

}
