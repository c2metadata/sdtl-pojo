package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Describes a criterion by which cases are sorted, including the variable name
 * and whether to sort ascending or descending.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SortCriterion {

	/**
	 * the variable used to sort. 1..1
	 */
	private VariableReferenceBase variable;

	/**
	 * The direction in which to sort. 1..1. Enumeration: Ascending, Descending
	 */
	private String sortDirection;

	public VariableReferenceBase getVariable() {
		return variable;
	}

	public void setVariable(VariableReferenceBase variable) {
		this.variable = variable;
	}

	public String getSortDirection() {
		return sortDirection;
	}

	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}

}
