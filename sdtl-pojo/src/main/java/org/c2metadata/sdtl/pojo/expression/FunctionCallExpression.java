/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.expression;

import org.c2metadata.sdtl.pojo.FunctionArgument;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * An expression evaluated by reference to the Function Library.
 * 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "$type")
public class FunctionCallExpression extends ExpressionBase {

	public static final String TYPE = "FunctionCallExpression";

	/**
	 * The name of the function being called. 1..1
	 */
	private String function;
	/**
	 * When true, the Function property contains the name of a function from the
	 * SDTL function library. When false, the Function property contains the
	 * name of a system-specific or user-defined function.
	 * 
	 */
	private boolean isSdtlName;

	/**
	 * A list of parameters to the function. 0...n
	 */
	private FunctionArgument[] arguments;

	public String getFunction() {
		return function;
	}

	public void setFunction(String function) {
		this.function = function;
	}

	/**
	 * When true, the Function property contains the name of a function from the
	 * SDTL function library. When false, the Function property contains the
	 * name of a system-specific or user-defined function.
	 * 
	 * @return
	 */
	@JsonProperty("isSdtlName")
	public boolean isSdtlName() {
		return isSdtlName;
	}

	/**
	 * When true, the Function property contains the name of a function from the
	 * SDTL function library. When false, the Function property contains the
	 * name of a system-specific or user-defined function.
	 * 
	 * @param isSdtlName
	 */
	@JsonProperty("isSdtlName")
	public void setIsSdtlName(boolean isSdtlName) {
		this.isSdtlName = isSdtlName;
	}

	public FunctionArgument[] getArguments() {
		return arguments;
	}

	public void setArguments(FunctionArgument[] arguments) {
		this.arguments = arguments;
	}

}