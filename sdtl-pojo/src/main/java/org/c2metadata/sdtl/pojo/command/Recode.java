/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.RecodeRule;
import org.c2metadata.sdtl.pojo.RecodeVariable;
import org.c2metadata.sdtl.pojo.expression.VariableRangeExpression;

/**
 * Describes recoding values in one or more variables according to a specified
 * mapping.
 * 
 * The Recode command can either describe a recoding of one or more individual
 * variables, or a range of variables. When one or more individual variables are
 * described, a new variable name can be specified. In this case, the original
 * variable is left alone, and a new variable is created with the recoded
 * values.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Recode extends TransformBase {

	public static final String TYPE = "Recode";

	/**
	 * The variables that will have their values recoded. The resulting values
	 * may be either stored in the same variable, or a newly created destination
	 * variable. 0..n
	 */
	private RecodeVariable[] recodedVariables;

	/**
	 * A range of variables to which the recode rules are applied. The resulting
	 * values are stored in the same variable. 0..1
	 */
	private VariableRangeExpression recodedVariableRange;

	/**
	 * A mapping describing which values will be recoded to which new values.
	 * 0..n
	 */
	private RecodeRule[] rules;

	public RecodeVariable[] getRecodedVariables() {
		return recodedVariables;
	}

	public void setRecodedVariables(RecodeVariable[] recodedVariables) {
		this.recodedVariables = recodedVariables;
	}

	public VariableRangeExpression getRecodedVariableRange() {
		return recodedVariableRange;
	}

	public void setRecodedVariableRange(VariableRangeExpression recodedVariableRange) {
		this.recodedVariableRange = recodedVariableRange;
	}

	public RecodeRule[] getRules() {
		return rules;
	}

	public void setRules(RecodeRule[] rules) {
		this.rules = rules;
	}

}