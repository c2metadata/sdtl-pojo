/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.expression;

import java.util.HashSet;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * This is the basis for all expressions. The Jackson annotations register all
 * the subtypes. If a expression is found that does not have a listed subtype,
 * it will default to deserializing it as an {@link UnknownExpression}.
 * 
 * The commented out subtypes should be uncommented if/when the model is edited
 * to include type fields on them. Until then they will default to being
 * UnknownExpressions
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, defaultImpl = UnknownExpression.class, property = "$type")
@JsonSubTypes({ 
	@JsonSubTypes.Type(value = BooleanConstantExpression.class, name = BooleanConstantExpression.TYPE),
		@JsonSubTypes.Type(value = FunctionCallExpression.class, name = FunctionCallExpression.TYPE),
		@JsonSubTypes.Type(value = GroupedExpression.class, name = GroupedExpression.TYPE),
		@JsonSubTypes.Type(value = IteratorSymbolExpression.class, name = IteratorSymbolExpression.TYPE),
		@JsonSubTypes.Type(value = MissingValueConstantExpression.class, name = MissingValueConstantExpression.TYPE),
		@JsonSubTypes.Type(value = NumberRangeExpression.class, name = NumberRangeExpression.TYPE),
		@JsonSubTypes.Type(value = NumericConstantExpression.class, name = NumericConstantExpression.TYPE),
		@JsonSubTypes.Type(value = NumericMaximumValueExpression.class, name = NumericMaximumValueExpression.TYPE),
		@JsonSubTypes.Type(value = NumericMinimumValueExpression.class, name = NumericMinimumValueExpression.TYPE),
		@JsonSubTypes.Type(value = StringConstantExpression.class, name = StringConstantExpression.TYPE),
		@JsonSubTypes.Type(value = StringRangeExpression.class, name = StringRangeExpression.TYPE),
		@JsonSubTypes.Type(value = UnhandledValuesExpression.class, name = UnhandledValuesExpression.TYPE),
		@JsonSubTypes.Type(value = ValueListExpression.class, name = ValueListExpression.TYPE),
		@JsonSubTypes.Type(value = VariableReferenceBase.class, name = VariableReferenceBase.TYPE),
		@JsonSubTypes.Type(value = VariableSymbolExpression.class, name = VariableSymbolExpression.TYPE),

		@JsonSubTypes.Type(value = TimeDurationConstant.class, name = TimeDurationConstant.TYPE),
		@JsonSubTypes.Type(value = DateTimeConstant.class, name = DateTimeConstant.TYPE),
		// @JsonSubTypes.Type(value = UnknownExpression.class, name =
		// UnknownExpression.TYPE),
		@JsonSubTypes.Type(value = UnsupportedExpression.class, name = UnsupportedExpression.TYPE) })

public class ExpressionBase {

	/**
	 * The name of the argument, when the expression is passed to a function.
	 */
	private String name;

	/**
	 * The name of this class, used for deserialization of derived types.
	 */
	private String typeName;

	/**
	 * A set to contain the names of unknown properties.
	 */
	private HashSet<String> unknownProperties;

	public ExpressionBase() {
		unknownProperties = new HashSet<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTypeName() {
		return typeName;
	}

	public void setTypeName(String typeName) {
		this.typeName = typeName;
	}

	/**
	 * Gets the name of all unknown properties that were encountered during
	 * deserialization.
	 * 
	 * @return the unknown property names
	 */
	public String[] getUnknownProperties() {
		return unknownProperties.toArray(new String[0]);
	}

	/**
	 * A fall back setter to capture an properties that are not known so that
	 * they can be logged.
	 * 
	 * @param property
	 *            the property name
	 * @param value
	 *            the property value; this will be ignored
	 */
	@JsonAnySetter
	public void addUnknownProperty(String property, Object value) {
		unknownProperties.add(property);
	}

}
