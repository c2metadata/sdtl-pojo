/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Describes how values will be recoded
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonIgnoreProperties(value = { "$type" })
public class RecodeRule {
	/**
	 * Values that are changed
	 */
	private ExpressionBase[] fromValue;

	/**
	 * The new value;
	 */
	private String to;

	/**
	 * A value label for the new recoded value, if appropriate.
	 */
	private String label;

	public ExpressionBase[] getFromValue() {
		return fromValue;
	}

	public void setFromValue(ExpressionBase[] fromValue) {
		this.fromValue = fromValue;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
