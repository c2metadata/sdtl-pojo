package org.c2metadata.sdtl.pojo.expression;

/**
 * A list of variables which may include variable names
 * {@link VariableSymbolExpression}, {@link CompositeVariableNameExpression},
 * and variable ranges {@linnk VariableRangeExpression}.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class VariableListExpression extends VariableReferenceBase {
	public static final String TYPE = "VariableListExpression";

	/**
	 * An array of variables and/or variable ranges. 1..n
	 */
	private VariableReferenceBase[] variables;

	public VariableReferenceBase[] getVariables() {
		return variables;
	}

	public void setVariables(VariableReferenceBase[] variables) {
		this.variables = variables;
	}

}
