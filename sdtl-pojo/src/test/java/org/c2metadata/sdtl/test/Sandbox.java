package org.c2metadata.sdtl.test;

import java.io.IOException;
import java.util.LinkedList;

import org.c2metadata.sdtl.pojo.DataframeDescription;
import org.c2metadata.sdtl.pojo.Program;
import org.c2metadata.sdtl.pojo.command.Analysis;
import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.c2metadata.sdtl.pojo.command.Recode;
import org.junit.Test;

import com.fasterxml.jackson.databind.ObjectMapper;

public class Sandbox {

	
	@Test
	public void dostuff() throws IOException{
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		
		Analysis analysis  = new Analysis();
		analysis.setCommand("analysis");
//		analysis.setTestProperty("blahblah");
		
		Recode recode = new Recode();
		recode.setCommand("recodee");
		DataframeDescription dd = new DataframeDescription();
		dd.setDataframeName("1");
//		recode.setConsumesDataframe(dd);
		
		String analysisString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(analysis);
		System.out.println(analysisString);
		String recodeeString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(recode);
		System.out.println(recodeeString);
		CommandBase base = mapper.readValue(analysisString, CommandBase.class);
		CommandBase recodeBase = mapper.readValue(recodeeString, CommandBase.class);
		System.out.println("ok");
	}
	
	@Test
	public void dostuff2() throws IOException{
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		Program p = new Program();
		Analysis analysis  = new Analysis();
		analysis.setCommand("analysis");
//		analysis.setTestProperty("blahblah");
		
		Recode recode = new Recode();
		recode.setCommand("recodee");
		DataframeDescription dd = new DataframeDescription();
		dd.setDataframeName("1");
//		recode.setConsumesDataframe(dd);
		
		String analysisString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(analysis);
		System.out.println(analysisString);
		String recodeeString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(recode);
//		System.out.println(recodeeString);
		CommandBase base = mapper.readValue(analysisString, CommandBase.class);
		CommandBase recodeBase = mapper.readValue(recodeeString, CommandBase.class);
//		System.out.println("ok");
		LinkedList<CommandBase> bases = new LinkedList<>();
		bases.add(analysis);
//		bases.add(recode);
		p.setCommands(bases);
		String pstring = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(p);
		System.out.println(pstring);
		Program reprogram = mapper.readValue(pstring, Program.class);
	}
	
}
