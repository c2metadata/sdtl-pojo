package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Describes a new variable created by reshaping a dataset from wide to long or
 * vice versa.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class ReshapeItemDescription {
	/**
	 * Name of new variable created by this command.
	 */
	private String targetVariableName;
	/**
	 * Label for new variable created by this command.
	 */
	private String targetVariableLabel;
	/**
	 * Source variables in the original dataset used to create this variable.
	 */
	private VariableReferenceBase sourceVariables;
	/**
	 * The stub is a string used in the names of variables in the wide dataset
	 */
	private String stub;
	/**
	 * A list of values that produce new rows (long) or columns (wide) for this
	 * variable.
	 */
	private ExpressionBase indexValues;
	/**
	 * Name of the variable that will contain the value of the index for this
	 * row
	 */
	private String indexVariableName;
	/**
	 * Label for the variable in IndexVariableName.
	 */
	private String indexVariableLabel;

	public String getTargetVariableName() {
		return targetVariableName;
	}

	public void setTargetVariableName(String targetVariableName) {
		this.targetVariableName = targetVariableName;
	}

	public String getTargetVariableLabel() {
		return targetVariableLabel;
	}

	public void setTargetVariableLabel(String targetVariableLabel) {
		this.targetVariableLabel = targetVariableLabel;
	}

	public VariableReferenceBase getSourceVariables() {
		return sourceVariables;
	}

	public void setSourceVariables(VariableReferenceBase sourceVariables) {
		this.sourceVariables = sourceVariables;
	}

	public String getStub() {
		return stub;
	}

	public void setStub(String stub) {
		this.stub = stub;
	}

	public ExpressionBase getIndexValues() {
		return indexValues;
	}

	public void setIndexValues(ExpressionBase indexValues) {
		this.indexValues = indexValues;
	}

	public String getIndexVariableName() {
		return indexVariableName;
	}

	public void setIndexVariableName(String indexVariableName) {
		this.indexVariableName = indexVariableName;
	}

	public String getIndexVariableLabel() {
		return indexVariableLabel;
	}

	public void setIndexVariableLabel(String indexVariableLabel) {
		this.indexVariableLabel = indexVariableLabel;
	}

}
