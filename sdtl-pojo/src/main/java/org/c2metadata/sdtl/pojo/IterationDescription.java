package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Describes an iteration process consisting of an IteratorSymbolExpression and
 * a list of values it takes.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
// ignoring bc this isn't a transform base.
@JsonIgnoreProperties(value = { "$type" })
public class IterationDescription {

	/**
	 * The name used in IteratorCommands for the index variable that changes
	 * value in the loop 1..1
	 * 
	 * In the docs, this is an iterator symbol expression, but it won't
	 * deserialize unless we make it an expression base.
	 */
	// private IteratorSymbolExpression iteratorSymbolName;
	private ExpressionBase iteratorSymbolName;

	/**
	 * Describes the values that are substituted for the index variable that
	 * changes value in the loop. 1..n
	 */
	private ExpressionBase[] iteratorValues;

	public ExpressionBase getIteratorSymbolName() {
		return iteratorSymbolName;
	}

	public void setIteratorSymbolName(ExpressionBase iteratorSymbolName) {
		this.iteratorSymbolName = iteratorSymbolName;
	}

	public ExpressionBase[] getIteratorValues() {
		return iteratorValues;
	}

	public void setIteratorValues(ExpressionBase[] iteratorValues) {
		this.iteratorValues = iteratorValues;
	}

}
