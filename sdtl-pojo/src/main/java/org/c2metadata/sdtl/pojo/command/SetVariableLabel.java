/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Describes the assignment of a label to a variable
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SetVariableLabel extends TransformBase {

	public static final String TYPE = "SetVariableLabel";

	/**
	 * The name of the variable to which a label will be assigned. 1..1
	 */
	private VariableReferenceBase variable;
	/**
	 * The label to be assigned to the variable. 1..1
	 */
	private String label;

	public VariableReferenceBase getVariable() {
		return variable;
	}

	public void setVariable(VariableReferenceBase variable) {
		this.variable = variable;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
