/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * Assigns the value of an expression to a variable. 
 * 
 * Re: annotation: The Aggregate pojo requires a list of Computes, but it was
 * not deserializing them correctly, as it was pulling in the defaultImpl of
 * UnknownTransform from the TransformBase class, seeing that UnknownTransform
 * was not a subclass of Compute, then throwing an error.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, defaultImpl = JsonTypeInfo.class, property = "$type", visible = true)
public class Compute extends TransformBase {

	public static final String TYPE = "Compute";

	/**
	 * The variable that is computed. 1..1
	 */
	private VariableReferenceBase variable;
	/**
	 * the expression used to compute the value of the variable(s). 1..1
	 **/
	private ExpressionBase expression;

	public VariableReferenceBase getVariable() {
		return variable;
	}

	public void setVariable(VariableReferenceBase variable) {
		this.variable = variable;
	}

	public ExpressionBase getExpression() {
		return expression;
	}

	public void setExpression(ExpressionBase expression) {
		this.expression = expression;
	}
}
