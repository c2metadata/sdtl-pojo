package org.c2metadata.sdtl.pojo.expression;

/**
 * Defines a range of string values, such as "A to N".
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class StringRangeExpression extends ExpressionBase {

	public static final String TYPE = "StringRangeExpression";
	/**
	 * Staring value for range. 1..1
	 */
	public String rangeStart;

	/**
	 * Ending value for range. 1..1
	 */
	public String rangeEnd;

	public String getRangeStart() {
		return rangeStart;
	}

	public void setRangeStart(String rangeStart) {
		this.rangeStart = rangeStart;
	}

	public String getRangeEnd() {
		return rangeEnd;
	}

	public void setRangeEnd(String rangeEnd) {
		this.rangeEnd = rangeEnd;
	}

	

}
