package org.c2metadata.sdtl.test;

import java.io.File;
import java.io.IOException;

import org.c2metadata.sdtl.pojo.Program;
import org.c2metadata.sdtl.pojo.ProgramMessage;
import org.c2metadata.sdtl.pojo.command.CommandBase;
import org.junit.Ignore;
import org.junit.Test;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SerializationTest {

	// serialize the expr implementation, then deserialize it and make sure it
	// goes back into the same class.

	@Test
	public void testSerialization() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

		// read from file to java
		File f = new File("src/test/resources/sdtlJson/sdtl_from_test.json");
		Program program = mapper.readValue(f, Program.class);
		// write java to json
		String json = mapper.writeValueAsString(program);
		// read it back in
		Program finalProgram = mapper.readValue(json, Program.class);
		System.out.print(mapper.writeValueAsString(finalProgram));
		for (CommandBase tb : finalProgram.getCommands()) {
			System.out.println(tb.getCommand());
		}
	}

	@Ignore // SDTL out of date
	@Test
	public void testSerialization2() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

		// read from file to java
		File f = new File("src/test/resources/sdtlJson/message.json");
		Program program = mapper.readValue(f, Program.class);
		ProgramMessage m = new ProgramMessage();
		m.setMessageText(new String[] { "testing" });
		program.addMessages(m);

		// write java to json
		String json = mapper.writeValueAsString(program);
		// read it back in
		Program finalProgram = mapper.readValue(json, Program.class);
		for (CommandBase tb : finalProgram.getCommands()) {
			System.out.println(tb.getCommand());
		}

	}
	
	@Test
	public void testAppendFileDescSerialization() throws JsonGenerationException, JsonMappingException, IOException {
		ObjectMapper mapper = new ObjectMapper();
		mapper.findAndRegisterModules();
		mapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);
		mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false); 		// read from file to java
		File f = new File("src/test/resources/sdtlJson/appendFileDesc.json");
		Program program = mapper.readValue(f, Program.class);
		// write java to json
		String json = mapper.writeValueAsString(program);
		// read it back in
		Program finalProgram = mapper.readValue(json, Program.class);
		System.out.print(mapper.writeValueAsString(finalProgram));
		for (CommandBase tb : finalProgram.getCommands()) {
			System.out.println(tb.getCommand());
		}
	}

	


}
