package org.c2metadata.sdtl.pojo.expression;
/**
 * Represents the smallest numeric value supported by a system.
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NumericMinimumValueExpression extends ExpressionBase{
	public static final String TYPE = "NumericMinimumValueExpression";

}
