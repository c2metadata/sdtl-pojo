package org.c2metadata.sdtl.pojo;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

/**
 * A composite variable name is used to create multiple variables from a common
 * 'stub' and a prefix or postfix, such as "var1", "var2", etc.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class CompositeVariableName extends ExpressionBase {

	public static final String TYPE = "CompositeVariableName";

	/**
	 * A string used as the base for forming new variable names
	 */
	private String stub;
	/**
	 * A string attached to the beginning of the Stub to form new variable
	 * names, e.g. '1new', '2new'.
	 */
	private ExpressionBase prefix;
	/**
	 * A string attached to the end of the Stub to form new variable names, e.g.
	 * 'new1', 'new2'.
	 */
	private ExpressionBase postfix;

	public String getStub() {
		return stub;
	}

	public void setStub(String stub) {
		this.stub = stub;
	}

	public ExpressionBase getPrefix() {
		return prefix;
	}

	public void setPrefix(ExpressionBase prefix) {
		this.prefix = prefix;
	}

	public ExpressionBase getPostfix() {
		return postfix;
	}

	public void setPostfix(ExpressionBase postfix) {
		this.postfix = postfix;
	}

}
