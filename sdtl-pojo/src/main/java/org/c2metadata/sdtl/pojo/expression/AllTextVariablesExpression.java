package org.c2metadata.sdtl.pojo.expression;

/**
 * An expression that represents all text variables in the dataset, similar to
 * _all in SPSS or Stata.
 * 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class AllTextVariablesExpression extends VariableReferenceBase {
	public static final String TYPE = "AllTextVariablesExpression";

}
