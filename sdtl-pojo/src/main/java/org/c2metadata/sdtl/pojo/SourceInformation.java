/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * SourceInformation defines information about the original source of a data transform.
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonIgnoreProperties(value = { "$type" })
public class SourceInformation {
	/**
	 * The line number of the beginning of the transform code
	 */
	private int lineNumberStart;
	/**
	 * The line number of the end of the transform code
	 */
	private int lineNumberEnd;
	/**
	 * The character index of the beginning of the transform code
	 */
	private int sourceStartIndex;
	/**
	 * The character index of the end of the transform code
	 */
	private int sourceStopIndex;
	/**
	 * The original source code of the data transform code
	 */
	private String originalSourceText;
	/**
	 * The original source code of the data transform code
	 */
	private String processedSourceText;

	public int getLineNumberStart() {
		return lineNumberStart;
	}

	public void setLineNumberStart(int lineNumberStart) {
		this.lineNumberStart = lineNumberStart;
	}

	public int getLineNumberEnd() {
		return lineNumberEnd;
	}

	public void setLineNumberEnd(int lineNumberEnd) {
		this.lineNumberEnd = lineNumberEnd;
	}

	public int getSourceStartIndex() {
		return sourceStartIndex;
	}

	public void setSourceStartIndex(int sourceStartIndex) {
		this.sourceStartIndex = sourceStartIndex;
	}

	public int getSourceStopIndex() {
		return sourceStopIndex;
	}

	public void setSourceStopIndex(int sourceStopIndex) {
		this.sourceStopIndex = sourceStopIndex;
	}

	public String getOriginalSourceText() {
		return originalSourceText;
	}

	public void setOriginalSourceText(String originalSourceText) {
		this.originalSourceText = originalSourceText;
	}

	public String getProcessedSourceText() {
		return processedSourceText;
	}

	public void setProcessedSourceText(String processedSourceText) {
		this.processedSourceText = processedSourceText;
	}

}
