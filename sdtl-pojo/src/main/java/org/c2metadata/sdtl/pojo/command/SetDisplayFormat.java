/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Sets the display or output format for a variable. Examples:
 * 
 * <pre>
 * 	"displayFormatSchema": "SAS"
	"displayFormatName": "DOLLAR6.2"
	
	"displayFormatSchema": "Stata 15.1"
	"displayFormatName": "%tcDDmonCCYY_HH:MM:SS"
 * </pre>
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SetDisplayFormat extends TransformBase {

	public static final String TYPE = "SetDisplayFormat";

	/**
	 * The list of variables that will have their format set. (1..n)
	 */
	private VariableReferenceBase[] variables;

	/**
	 * A vendor or standards body with a controlled vocabulary. The value can be
	 * a URL.. 0..1
	 * 
	 * Note: the spss var types are f8.2, comma9.2, dot9.2, dollar10.2, pct9.2,
	 * e8.1 - need stata types? will it be type specific? from:
	 * https://www.gnu.org/software/pspp/manual/html_node/Basic-Numeric-Formats.html#Basic-Numeric-Formats
	 */
	private String displayFormatSchema;

	/**
	 * The name used in the associated schema. 0...1
	 */
	private String displayFormatName;

	public VariableReferenceBase[] getVariables() {
		return variables;
	}

	public void setVariables(VariableReferenceBase[] variables) {
		this.variables = variables;
	}

	public String getDisplayFormatSchema() {
		return displayFormatSchema;
	}

	public void setDisplayFormatSchema(String displayFormatSchema) {
		this.displayFormatSchema = displayFormatSchema;
	}

	public String getDisplayFormatName() {
		return displayFormatName;
	}

	public void setDisplayFormatName(String displayFormatName) {
		this.displayFormatName = displayFormatName;
	}

}
