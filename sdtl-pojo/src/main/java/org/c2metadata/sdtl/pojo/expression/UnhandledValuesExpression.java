package org.c2metadata.sdtl.pojo.expression;

/**
 * Represents any values not previously handled (for example, in a set of recode
 * rules).
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class UnhandledValuesExpression extends ExpressionBase {
	
	public static final String TYPE = "UnhandledValuesExpression";

}
