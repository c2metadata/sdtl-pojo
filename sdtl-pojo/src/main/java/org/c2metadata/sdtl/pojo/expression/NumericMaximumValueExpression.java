package org.c2metadata.sdtl.pojo.expression;
/**
 * Represents the largest numeric value supported by a system.
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NumericMaximumValueExpression extends ExpressionBase{
	public static final String TYPE = "NumericMaximumValueExpression";

}
