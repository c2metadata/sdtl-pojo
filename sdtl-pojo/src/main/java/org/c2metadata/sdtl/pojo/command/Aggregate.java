package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.Weight;
import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;
import org.c2metadata.sdtl.pojo.expression.VariableSymbolExpression;

/**
 * An aggregation summarizes data using aggregation functions applied to data
 * that may be grouped by one or more variables. The resulting summary data is
 * added to each row of the existing dataset. See the aggregation functions in
 * the Function Library.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Aggregate extends TransformBase {

	public static final String TYPE = "Aggregate";

	/**
	 * Variables used as keys to identify groups 0...n
	 */
	private VariableReferenceBase[] groupByVariables;
	/**
	 * The expressions that compute the aggregations. An aggregation function
	 * should be used. 1...n
	 */
	private Compute[] aggregateVariables;
	/**
	 * Description of the weights to be used. 0..1
	 */
	private Weight weighting;

	public VariableReferenceBase[] getGroupByVariables() {
		return groupByVariables;
	}

	public void setGroupByVariables(VariableReferenceBase[] groupByVariables) {
		this.groupByVariables = groupByVariables;
	}

	public Compute[] getAggregateVariables() {
		return aggregateVariables;
	}

	public void setAggregateVariables(Compute[] aggregateVariables) {
		this.aggregateVariables = aggregateVariables;
	}

	public Weight getWeighting() {
		return weighting;
	}

	public void setWeighting(Weight weighting) {
		this.weighting = weighting;
	}

}
