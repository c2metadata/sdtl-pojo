package org.c2metadata.sdtl.pojo.expression;

/**
 * The name of an iterator symbol used as an index in describing the actions of
 * a loop.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */

public class IteratorSymbolExpression extends ExpressionBase {

	public static final String TYPE = "IteratorSymbolExpression";

	/**
	 * A name used for the index variable that takes different values inside a
	 * loop.
	 */
	private String name;
	private String type;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
}
