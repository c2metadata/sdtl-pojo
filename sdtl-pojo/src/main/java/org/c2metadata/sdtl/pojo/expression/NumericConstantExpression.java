package org.c2metadata.sdtl.pojo.expression;

/**
 * A numeric constant
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class NumericConstantExpression extends ExpressionBase {
	
	public static final String TYPE = "NumericConstantExpression";
	/**
	 * A number. 1..1
	 */
	private String value;
	/**
	 * Type of the number. 1..1
	 */
	private String numericType;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNumericType() {
		return numericType;
	}

	public void setNumericType(String numericType) {
		this.numericType = numericType;
	}

}
