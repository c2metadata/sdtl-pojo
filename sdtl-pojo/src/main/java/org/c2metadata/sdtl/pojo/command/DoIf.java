/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.ExpressionBase;

/**
 * A set of commands that are performed when a logical expression is true. May
 * also include ElseCommands to be performed if the logical expression is false.
 * The commands in DoIf are performed once, and it expects a logical condition
 * that applies to the entire dataframe. Use IfRows for commands that are
 * performed on each row depending upon values on those rows.
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class DoIf extends TransformBase {

	public static final String TYPE = "DoIf";
	/**
	 * A logical expression. 1..1
	 */
	private ExpressionBase condition;
	/**
	 * Commands to be performed if the condition is true. 0..n
	 */
	private TransformBase[] thenCommands;

	/**
	 * Commands to be performed if the condition is false. 0..n
	 */
	private TransformBase[] elseCommands;

	public ExpressionBase getCondition() {
		return condition;
	}

	public void setCondition(ExpressionBase condition) {
		this.condition = condition;
	}

	public TransformBase[] getThenCommands() {
		return thenCommands;
	}

	public void setThenCommands(TransformBase[] thenCommands) {
		this.thenCommands = thenCommands;
	}

	public TransformBase[] getElseCommands() {
		return elseCommands;
	}

	public void setElseCommands(TransformBase[] elseCommands) {
		this.elseCommands = elseCommands;
	}

}
