package org.c2metadata.sdtl.pojo.expression;

import java.time.Duration;

public class TimeDurationConstant extends ExpressionBase{
	public static final String TYPE = "TimeDurationConstant";

	/**
	 * A duration of time in ISO 8601 format

	 */
	private Duration timeDurationValue;
	
	public TimeDurationConstant() {
		// TODO Auto-generated constructor stub
	}
	public Duration getTimeDurationValue() {
		return timeDurationValue;
	}
	
	public void setTimeDurationValue(Duration timeDurationValue) {
		this.timeDurationValue = timeDurationValue;
	}
}
