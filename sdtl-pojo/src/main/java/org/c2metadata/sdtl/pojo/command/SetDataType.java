/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo.command;

import org.c2metadata.sdtl.pojo.expression.VariableReferenceBase;

/**
 * Sets the data type of a variable. Examples:
 * 
 * <pre>
 *  "dataType": "Text",
	"subTypeSchema": "Stata 15.1",
	"subType": "str1"
 * 
 *  "dataType": "Numeric",
	"subTypeSchema": "https://www.ddialliance.org/Specification/DDI-CV/DataType_1.0.html" ,
	"subType": "Int"
 * 
 * </pre>
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class SetDataType extends TransformBase {

	public static final String TYPE = "SetDataType";

	/**
	 * The variables that will have their type set. 0...n
	 */
	private VariableReferenceBase[] variables;

	/**
	 * General type of a variable, e.g. “Text” or “Numeric”. 1...n
	 * 
	 * Enumeration: Text, numeric, boolean, date-time, factor
	 */
	private String dataType;

	/**
	 * A vendor or standards body with a controlled vocabulary. The value can be
	 * a URL. 0...1
	 */
	private String subTypeSchema;

	/**
	 * The name used in the associated schema. 0...1
	 */
	private String subType;

	public VariableReferenceBase[] getVariables() {
		return variables;
	}

	public void setVariables(VariableReferenceBase[] variables) {
		this.variables = variables;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getSubTypeSchema() {
		return subTypeSchema;
	}

	public void setSubTypeSchema(String subTypeSchema) {
		this.subTypeSchema = subTypeSchema;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

}
