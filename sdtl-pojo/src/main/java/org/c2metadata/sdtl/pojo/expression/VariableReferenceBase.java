package org.c2metadata.sdtl.pojo.expression;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.As;

/**
 * Types used to describe variables in expressions.

 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = As.PROPERTY, property = "$type")
@JsonSubTypes({
	@JsonSubTypes.Type(value = VariableReferenceBase.class, name = VariableReferenceBase.TYPE),
	@JsonSubTypes.Type(value = AllNumericVariablesExpression.class, name = AllNumericVariablesExpression.TYPE),
	@JsonSubTypes.Type(value = AllTextVariablesExpression.class, name = AllTextVariablesExpression.TYPE),
	@JsonSubTypes.Type(value = AllVariablesExpression.class, name = AllVariablesExpression.TYPE),
	@JsonSubTypes.Type(value = CompositeVariableNameExpression.class, name = CompositeVariableNameExpression.TYPE),
	@JsonSubTypes.Type(value = VariableListExpression.class, name = VariableListExpression.TYPE),
	@JsonSubTypes.Type(value = VariableRangeExpression.class, name = VariableRangeExpression.TYPE),
	@JsonSubTypes.Type(value = VariableSymbolExpression.class, name = VariableSymbolExpression.TYPE)})
public class VariableReferenceBase extends ExpressionBase {

	public static final String TYPE = "VariableReferenceBase";


}
