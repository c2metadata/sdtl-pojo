/**
<Overview>
 
Copyright 2017 Metadata Technology North America Inc.
 
This software is the result of collaborative efforts from all participants of the C2Metadata project (http://www.c2metadata.org)
 
C2Metadata is supported by the Data Infrastructure Building Blocks (DIBBs) program of the National Science Foundation through grant NSF ACI-1640575.
 
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at
 
    http://www.apache.org/licenses/LICENSE-2.0
 
Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
 
**/
package org.c2metadata.sdtl.pojo;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.c2metadata.sdtl.pojo.command.CommandBase;


/**
 * The program represents a list of commands from a statistical package.
 * 
 * 
 * @author Carson Hunter (carson.hunter@mtna.us)
 *
 */
public class Program {

	/**
	 * ID of the object being referenced.
	 */
	private String id;
	/**
	 * The name of the file containing the source code.
	 */
	private String sourceFileName;
	/**
	 * The language of the source code.
	 */
	private String sourceLanguage;
	/**
	 * The MD5 hash of the contents of the file.
	 */
	private String scriptMD5;
	/**
	 * The SHA-1 hash of the contents of the file.
	 */
	private String scriptSHA1;
	/**
	 * The date and time the file was last updated
	 */
	private Date sourceFileLastUpdate;
	 /**
	  * The size of the file in bytes
	  */
	private Long sourceFileSize;
	/**
	  * The number of lines in the source file
	  */
	private int lineCount;
	/**
	  * The number of commands detected in the source file
	  */
	private int commandCount;
	/**
	  * The name of the parser used to generate the SDTL
	  */
	private String parser;
	/**
	  * The version of the parser used to generate the SDTL.
	  */
	private String parserVersion;
	/**
	  * The version of the SDTL model.
	  */
	private String modelVersion;
	/**
	  * The date and time the SDTL was generated.
	  */
	private Date modelCreatedTime;
	/**
	 * The list of commands that make up the program.
	 */
	private LinkedList<CommandBase> commands;
	/**
	 * Messages related to the parsing of the source file.
	 */
	private List<ProgramMessage> messages;

	public Program() {
		this.commands = new LinkedList<>();
		this.messages = new ArrayList<>();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

	public String getSourceLanguage() {
		return sourceLanguage;
	}

	public void setSourceLanguage(String sourceLanguage) {
		this.sourceLanguage = sourceLanguage;
	}

	public LinkedList<CommandBase> getCommands() {
		return commands;
	}

	public void setCommands(LinkedList<CommandBase> commands) {
		this.commands = commands;
	}

	public String getScriptMD5() {
		return scriptMD5;
	}

	public void setScriptMD5(String scriptMD5) {
		this.scriptMD5 = scriptMD5;
	}

	public String getScriptSHA1() {
		return scriptSHA1;
	}

	public void setScriptSHA1(String scriptSHA1) {
		this.scriptSHA1 = scriptSHA1;
	}

	public Date getSourceFileLastUpdate() {
		return sourceFileLastUpdate;
	}

	public void setSourceFileLastUpdate(Date sourceFileLastUpdate) {
		this.sourceFileLastUpdate = sourceFileLastUpdate;
	}

	public Long getSourceFileSize() {
		return sourceFileSize;
	}

	public void setSourceFileSize(Long sourceFileSize) {
		this.sourceFileSize = sourceFileSize;
	}

	public int getLineCount() {
		return lineCount;
	}

	public void setLineCount(int lineCount) {
		this.lineCount = lineCount;
	}

	public int getCommandCount() {
		return commandCount;
	}

	public void setCommandCount(int commandCount) {
		this.commandCount = commandCount;
	}

	public String getParser() {
		return parser;
	}

	public void setParser(String parser) {
		this.parser = parser;
	}

	public String getParserVersion() {
		return parserVersion;
	}

	public void setParserVersion(String parserVersion) {
		this.parserVersion = parserVersion;
	}

	public String getModelVersion() {
		return modelVersion;
	}

	public void setModelVersion(String modelVersion) {
		this.modelVersion = modelVersion;
	}

	public Date getModelCreatedTime() {
		return modelCreatedTime;
	}

	public void setModelCreatedTime(Date modelCreatedTime) {
		this.modelCreatedTime = modelCreatedTime;
	}

	public List<ProgramMessage> getMessages() {
		return messages;
	}

	public void setMessages(List<ProgramMessage> messages) {
		this.messages = messages;
	}

	public void addMessages(ProgramMessage...messages){
		for(ProgramMessage m : messages){
			this.messages.add(m);
		}
	}
}
